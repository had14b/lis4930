> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4930 - Special Topics: Advanced Mobile Web Application Development

## Homer Davis

### Project 2 Requirements:

*Five Parts:*

1. Include splash screen (optional 10 additional pts.).
2. Insert at least five sample tasks (see p. 413, and screenshot below).
3. Test database class (see pp. 424, 425)
4. Must add background color(s) or theme
5. Create and display launcher icon image.

#### README.md file should include the following items:

* Screenshot of running application’s Task List 

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

*Screenshot of running application’s Task List*

![Screenshot of running application’s Task List](img/main.PNG)