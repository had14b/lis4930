> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4930 - Special Topics: Advanced Mobile Web Application Development

## Homer Davis

### Assignment 5 Requirements:

*Four Parts:*

1. Include splash screen with app title and list of articles.
2. Must find and use your own RSS feed.
3. Must add background color(s) or theme
4. Create and display launcher icon image


#### README.md file should include the following items:

* Screenshot of running application’s splash screen
* Screenshot of running application’s individual article
* Screenshot of running application’s default browser

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

|*Screenshot of Splash Screen*|*Screenshot of Individual Article*|
-------------------------------|--------------------------------|
|![Splash screenshot](img/splash.PNG)|![Individual Article screenshot](img/item.PNG)|

|*Screenshot of Default Browser*|  |
-------------------------------|--------------------------------|
|![Default Browser screenshot](img/web.PNG)| |


