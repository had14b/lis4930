> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4930 - Special Topics: Advanced Mobile Web Application Development

## Homer Davis

### Assignment 3 Requirements:

*Five Parts:*

1. Work through Chapter 6 Tutorial
2. Backwards Engineer provided screenshots
3. Create Splash Screen
4. Create Follow-up screen (with images and buttons)
5. Allow playable media when buttons are clicked

#### README.md file should include the following items:

* Screenshot of running application’s splash screen
* Screenshot of running application’s follow-up screen (with images and buttons)
* Screenshots of running application’s play and pause user interfaces (with images and buttons)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

|*Screenshot of Splash Screen*|*Screenshot of Follow-up Screen*|
-------------------------------|--------------------------------|
|![Screenshot of Splash Screen](img/splash.PNG)|![Screenshot of Follow-up Screen](img/main.PNG)|

|*Screenshot of Playing Media*|*Screenshot of Paused Media*|
-------------------------------|--------------------------------|
|![Screenshot of Playing Media](img/playing.PNG)|![Screenshot of Paused Media](img/paused.PNG)|