package com.example.mymusic;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button buttonSP, buttonPhinehas, buttonEnd;
    MediaPlayer mpSP, mpPhinehas, mpEnd;
    int playing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        buttonSP = findViewById(R.id.btnSP);
        buttonPhinehas = findViewById(R.id.btnPhinehas);
        buttonEnd = findViewById(R.id.btnEnd);
        buttonSP.setOnClickListener(bSP);
        buttonPhinehas.setOnClickListener(bPhinehas);
        buttonEnd.setOnClickListener(bEnd);
        mpSP = new MediaPlayer();
        mpSP = MediaPlayer.create(this, R.raw.sp);
        mpPhinehas = new MediaPlayer();
        mpPhinehas = MediaPlayer.create(this, R.raw.phinehas);
        mpEnd = new MediaPlayer();
        mpEnd= MediaPlayer.create(this, R.raw.end);
        playing = 0;
    }

    OnClickListener bSP = new OnClickListener(){
        @Override
        public void onClick(View v) {
            switch(playing) {
                case 0:
                    mpSP.start();
                    playing = 1;
                    buttonSP.setText("Pause Silent Planet");
                    buttonPhinehas.setVisibility(View.INVISIBLE);
                    buttonEnd.setVisibility(View.INVISIBLE);
                    break;
                case 1:
                    mpSP.pause();
                    playing = 0;
                    buttonSP.setText("Play Silent Planet");
                    buttonPhinehas.setVisibility(View.VISIBLE);
                    buttonEnd.setVisibility(View.VISIBLE);
                    break;
            }
        }
    };

    OnClickListener bPhinehas = new OnClickListener(){
        @Override
        public void onClick(View v) {
            switch(playing) {
                case 0:
                    mpPhinehas.start();
                    playing = 1;
                    buttonPhinehas.setText("Pause Phinehas");
                    buttonSP.setVisibility(View.INVISIBLE);
                    buttonEnd.setVisibility(View.INVISIBLE);
                    break;
                case 1:
                    mpPhinehas.pause();
                    playing = 0;
                    buttonPhinehas.setText("Play Phinehas");
                    buttonSP.setVisibility(View.VISIBLE);
                    buttonEnd.setVisibility(View.VISIBLE);
                    break;
            }
        }
    };

    OnClickListener bEnd = new OnClickListener(){
        @Override
        public void onClick(View v) {
            switch(playing) {
                case 0:
                    mpEnd.start();
                    playing = 1;
                    buttonEnd.setText("Pause END");
                    buttonPhinehas.setVisibility(View.INVISIBLE);
                    buttonSP.setVisibility(View.INVISIBLE);
                    break;
                case 1:
                    mpEnd.pause();
                    playing = 0;
                    buttonEnd.setText("Play END");
                    buttonPhinehas.setVisibility(View.VISIBLE);
                    buttonSP.setVisibility(View.VISIBLE);
                    break;
            }
        }
    };
}
