package com.example.tipcalculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.text.DecimalFormat;


public class MainActivity extends AppCompatActivity {
String guestChoice;
String percentChoice;
double billTotal;
double calculatedTotal;
double tip;
double calculatedTip;
int percent;
int guest;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.ic_launcher_round);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        final EditText bill = findViewById(R.id.editText);
        final Button calculate = findViewById(R.id.button);
        final Spinner staticSpinner = findViewById(R.id.spinner);


        // Create an ArrayAdapter using the string array and a default spinner
        ArrayAdapter<CharSequence> staticAdapter = ArrayAdapter
                .createFromResource(this, R.array.guest_array,
                        android.R.layout.simple_spinner_item);

        // Specify the layout to use when the list of choices appears
        staticAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Apply the adapter to the spinner
        staticSpinner.setAdapter(staticAdapter);



        final Spinner staticSpinner2 = findViewById(R.id.spinner2);

        // Create an ArrayAdapter using the string array and a default spinner
        ArrayAdapter<CharSequence> staticAdapter2 = ArrayAdapter
                .createFromResource(this, R.array.percent_array,
                        android.R.layout.simple_spinner_item);

        // Specify the layout to use when the list of choices appears
        staticAdapter2
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Apply the adapter to the spinner
        staticSpinner2.setAdapter(staticAdapter2);

        calculate.setOnClickListener(new View.OnClickListener() {
            final TextView result = (findViewById(R.id.textView2));
            @Override
            public void onClick(View view) {
                billTotal = Double.parseDouble(bill.getText().toString());
                guestChoice = staticSpinner.getSelectedItem().toString();
                percentChoice = staticSpinner2.getSelectedItem().toString();
                guest = Integer.parseInt(guestChoice);
                percent = Integer.parseInt(percentChoice);

                tip = percent / 100.00;
                calculatedTip = billTotal * tip;

                calculatedTotal = (calculatedTip + billTotal) / guest;


                DecimalFormat currency = new DecimalFormat("$###,###.00");
                result.setText("Cost for each of " + guest + " guests: " + currency.format(calculatedTotal));
                //result.setText(Double.toString(calculatedTotal));
            }
        });

















    }
}
