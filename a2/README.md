> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4930 - Special Topics: Advanced Mobile Web Application Development

## Homer Davis

### Assignment 2 Requirements:

*Four Parts:*

1. Work through Chapter 3 Tutorial
2. Backwards Engineer provided screenshots
3. Use dropdown menus to accept user input
4. Extra Credit: Include display launcher icon

#### README.md file should include the following items:

* Screenshot of running application’s unpopulated user interface
* Screenshot of running application’s populated user interface

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

|*Screenshot of Unpopulated UI*|*Screenshot of Populated UI*|
-------------------------------|--------------------------------|
|![Unpopulated app screenshot](img/unpop.PNG)|![Populated app screenshot](img/pop.PNG)|
