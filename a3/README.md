> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4930 - Special Topics: Advanced Mobile Web Application Development

## Homer Davis

### Assignment 3 Requirements:

*Five Parts:*

1. Work through Chapter 4 Tutorial
2. Backwards Engineer provided screenshots
3. Use radio buttons for conversions
4. Use toast notification for out-of-range values
5. Extra Credit: Include Splash/Loading Screen

#### README.md file should include the following items:

* Screenshot of running application’s unpopulated user interface
* Screenshot of running application’s populated user interface
* Screenshot of Toast Notification

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

|*Screenshot of Unpopulated UI*|*Screenshot of Populated UI*|
-------------------------------|--------------------------------|
|![Unpopulated app screenshot](img/unpop.PNG)|![Populated app screenshot](img/pop.PNG)|


*Screenshot of Toast Notification*

![Toast Notification screenshot](img/toast.PNG)