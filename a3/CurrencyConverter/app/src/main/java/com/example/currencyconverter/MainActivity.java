package com.example.currencyconverter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import java.text.DecimalFormat;


public class MainActivity extends AppCompatActivity {
    double euroConv = 0.8;
    double pesoConv = 18.52;
    double canadianConv = 1.25;
    double USdollars;
    double converted;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(true);


        final EditText dollarsEntered = findViewById(R.id.editText);
        final RadioButton UStoEU = findViewById(R.id.radUStoEU);
        final RadioButton UStoPeso = findViewById(R.id.radUStoPES);
        final RadioButton UStoCanadian = findViewById(R.id.radUStoCAN);
        Button convert = findViewById(R.id.btnConvert);
        final TextView result = findViewById(R.id.textView);

        convert.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                USdollars = Double.parseDouble(dollarsEntered.getText().toString());
                DecimalFormat dolPes = new DecimalFormat("$###,###");
                DecimalFormat euro = new DecimalFormat("€###,###");


                if (UStoEU.isChecked()){
                    if (USdollars <= 100000 && USdollars > 0)
                    {
                        converted = USdollars * euroConv;
                        result.setText(euro.format(converted) + " Euros");
                    } else
                    {
                        Toast.makeText(MainActivity.this,"US Dollars must be <= 100,000 and > 0", Toast.LENGTH_LONG).show();
                    }
                }


                else if (UStoPeso.isChecked()){
                    if (USdollars <= 100000 && USdollars > 0)
                    {
                        converted = USdollars * pesoConv;
                        result.setText(dolPes.format(converted) + " Pesos");
                    } else
                    {
                        Toast.makeText(MainActivity.this,"US Dollars must be <= 100,000 and > 0", Toast.LENGTH_LONG).show();
                    }
                }


                else if (UStoCanadian.isChecked()){
                    if (USdollars <= 100000 && USdollars > 0)
                    {
                        converted = USdollars * canadianConv;
                        result.setText(dolPes.format(converted) + " Canadian Dollars");
                    } else
                    {
                        Toast.makeText(MainActivity.this,"US Dollars must be <= 100,000 and > 0", Toast.LENGTH_LONG).show();
                    }
                }


            }

        });
    }
}
