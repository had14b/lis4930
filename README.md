> **NOTE:** A README.md file should be placed at the **root of each of your repos directories.**
>


# LIS4930 - Special Topics: Advanced Mobile Web Application Development

## Homer Davis

### LIS4930 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md)
    * Install JDK
    * Install Android Studio and create My First App and Contacts App
    * Provide screenshots of installations
    * Create Bitbucket repo
    * Complete Bitbucket tutorials (bitbucketstationlocations and myteamquotes)
    * Provide git command descriptions
2. [A2 README.md](a2/README.md)
    * Create Android app
    * Provide screenshots of completed app
3. [A3 README.md](a3/README.md)
    * Backwards Engineer provided screenshots
    * Use radio buttons for conversions
    * Use toast notification for out-of-range values
    * Extra Credit: Include Splash/Loading Screen
4. [A4 README.md](a4/README.md)
    * Backwards Engineer provided screenshots
    * Use persistent data: SharedPreferences
    * Use data validation
5. [A5 README.md](a5/README.md)
    * Include splash screen with app title and list of articles.
    * Must find and use your own RSS feed.
    * Must add background color(s) or theme
    * Create and display launcher icon image
6. [P1 README.md](p1/README.md)
    * Backwards Engineer provided screenshots
    * Create Splash Screen
    * Create Follow-up screen (with images and buttons)
    * Allow playable media when buttons are clicked
7. [P2 README.md](p2/README.md)
    * Include splash screen (optional 10 additional pts.).
    * Insert at least five sample tasks (see p. 413, and screenshot below).
    * Test database class (see pp. 424, 425)
    * Must add background color(s) or theme
    * Create and display launcher icon image.