> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4930 - Special Topics: Advanced Mobile Web Application Development

## Homer Davis

### Assignment 1 Requirements:

*Four Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter Questions (Ch 1,2)
4. Bitbucket repo links: a) this assignment and b) the completed tutorials above (bitbucketstationlocations and myteamquotes).

#### README.md file should include the following items:

* Screenshot of running java Hello
* Screenshot of running Android Studio - My First App
* Screenshot of running Android Studio - Contacts App
* git commands w/short descriptions

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init: Create an empty Git repository or reinitialize an existing one
2. git status: Show the working tree status
3. git add: Add file contents to the index
4. git commit: Record changes to the repository
5. git push: Update remote refs along with associated objects
6. git pull: Fetch from and integrate with another repository or a local branch
7. git clone: Clone a repository into a new directory

#### Assignment Screenshots:

*Screenshot of running java Hello* 

![java Hello screenshot](img/Hello.PNG)

*Screenshot of Android Studio - My First App*

![My First App screenshot](img/myFirstApp.PNG)

*Screenshot of Contacts App - Main Screen*

![Contacts App main screenshot](img/main.PNG)

*Screenshot of Contacts App - Info Screen*

![Contacts App info screenshot](img/info.PNG)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/had14b/bitbucketstationlocations "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/had14b/myteamquotes/ "My Team Quotes Tutorial")
