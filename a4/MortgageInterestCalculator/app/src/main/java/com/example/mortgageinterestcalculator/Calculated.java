package com.example.mortgageinterestcalculator;

import android.content.SharedPreferences;
import android.media.Image;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DecimalFormat;

public class Calculated extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculated);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        float decPayment = sharedPref.getFloat("key1", 0);
        int intYears = sharedPref.getInt("key2", 0);
        float decPrincipal = sharedPref.getFloat("key3", 0);
        float decTotalPaid;

        TextView totalPaid = findViewById(R.id.txtTotalPaid);
        ImageView image = findViewById(R.id.imgYears);

        if (intYears == 10 || intYears == 20 || intYears == 30) {
            decTotalPaid = (decPayment * 12 * intYears) - decPrincipal;
            DecimalFormat currency = new DecimalFormat("$###,###.00");
            totalPaid.setText("Total paid: " + currency.format(decTotalPaid));

            if (intYears == 10) {
                image.setImageResource(R.drawable.y10);
            } else if (intYears == 20) {
                image.setImageResource(R.drawable.y20);
            } else if (intYears == 30) {
                image.setImageResource(R.drawable.y30);
            }

        } else {
            image.setImageResource(R.drawable.mortgage);
            totalPaid.setText("Enter 10, 20, or 30 years");
        }


    }
}
