> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4930 - Special Topics: Advanced Mobile Web Application Development

## Homer Davis

### Assignment 4 Requirements:

*Eight Parts:*

1. Work through Chapter 11 Tutorial
2. Backwards Engineer provided screenshots
3. Include splash screen image, app title, intro text.
4. Include appropriate images.
5. Must use persistent data: SharedPreferences
6. Widgets and images must be vertically and horizontally aligned.
7. Must add background color(s) or theme
8. Create and display launcher icon image

#### README.md file should include the following items:

* Screenshot of running application’s splash screen
* Screenshot of running application’s unpopulated user interface
* Screenshot of running application’s populated user interface
* Screenshot of Data Validation
* Screenshot of Calculated Interest

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

|*Screenshot of Splash Screen*|*Screenshot of Unpopulated UI*|
-------------------------------|--------------------------------|
|![Splash screenshot](img/splash.PNG)|![Populated app screenshot](img/empty.PNG)|

|*Screenshot of Populated UI*|*Screenshot of Data Validation*|
-------------------------------|--------------------------------|
|![Populated app screenshot](img/full.PNG)|![Data Validation screenshot](img/invalid.PNG)|


*Screenshot of Calculated Interest*

![Calculated screenshot](img/calculated.PNG)

